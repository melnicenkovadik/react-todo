import {useState} from "react";

export const useTodos = initialState => {
    const [searchValue, setSearchValue] = useState("");
    const [dateFrom, setDateFrom] = useState('')
    const [dateTo, setDateTo] = useState('')
    const [todos, setTodos] = useState([
        {
            text: "Learn Reactjs today",
            isCompleted: false,
            date: new Date('Fri Apr 15 2021 03:00:00 GMT+0300')
        },
        {
            text: "Buy meat from the market",
            isCompleted: false,
            date: new Date('04-17-2021-10:00')
        },
        {
            text: "Go to friend's house",
            isCompleted: false,
            date: new Date('04-27-2021-10:00')
        }
    ]);
    const addTodo = text => {
        const newTodos = [...todos, {text, date: new Date(Date.now())}];
        setTodos(newTodos);
        setSearchTodos(newTodos);
    };
    const [searchTodos, setSearchTodos] = useState([...todos]);
    const searchHandler = text => {
        if (!searchValue.length || searchTodos === [] || todos) {
            setSearchTodos(todos);
        }
        const newTodos = [...todos]
        const filtered = newTodos.filter(item =>
            item.text.toLowerCase().trim()
                .indexOf(searchValue.toLowerCase().trim()) !== -1)
        setSearchTodos(filtered);
    };

    const completeTodo = index => {
        const newTodos = [...todos];
        newTodos[index].isCompleted = true;
        setTodos(newTodos);
    };

    const removeTodo = index => {
        searchHandler()
        const newTodos = [...todos];
        newTodos.splice(index, 1);
        setTodos(newTodos);
        setSearchTodos(newTodos);
    };

    const filterDate = date => {
        const newTodos = [...todos]
        setSearchTodos(newTodos.filter(todo => todo.date >= dateFrom && todo.date <= dateTo &&
            todo.text.toLowerCase().trim()
                .indexOf(searchValue.toLowerCase().trim()) !== -1))
    };
    return {
        filterDate, removeTodo, completeTodo, searchHandler, setSearchValue, setDateTo, searchTodos,
        addTodo, searchValue, setDateFrom, dateTo, dateFrom
    };
};
