import React from "react";
import Todo from "./Todo/Todo";
import TodoForm from "./Todo/TodoForm";
import Filtered from "./Todo/Filtered";
import {useTodos} from "../hooks/useTodos";

const App = () => {
    const {
        filterDate, removeTodo, completeTodo, searchHandler,
        setSearchValue, setDateTo, searchTodos,
        addTodo, searchValue, setDateFrom
    } = useTodos([]);

    const todo = {
        searchValue,
        setSearchValue,
        searchHandler,
        setDateFrom,
        setDateTo,
        filterDate
    }
    return (
        <div className="main">
            <div className="todo-list-holder">

                <Filtered {...todo}/>

                <TodoForm addTodo={addTodo}/>

                <ul className="todo-list">
                    {searchTodos.map((todo, index) => (
                        <Todo
                            key={index}
                            index={index}
                            completeTodo={completeTodo}
                            removeTodo={removeTodo}
                            todo={todo}
                        />
                    ))}
                </ul>

            </div>
        </div>
    );
};

export default App;
