import React, {useState} from "react";

const TodoForm = ({addTodo}) => {
    const [value, setValue] = useState("");

    const handleSubmit = event => {
        event.preventDefault();
        if (!value) return;
        addTodo(value);
        setValue("");
    };

    return (
        <div className="todo-form">
            <h4>Enter your todo list</h4>

            <form onSubmit={handleSubmit}>
                <div className="form-holder">
                    <input
                        type="text"
                        placeholder="Add todos..."
                        onChange={(event) => setValue(event.target.value)}
                        className="input-field"
                        value={value}
                    />
                </div>
            </form>
        </div>
    );
};

export default TodoForm;
