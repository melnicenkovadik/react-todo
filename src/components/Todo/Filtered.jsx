import React from "react";

const Filtered = ({searchValue, setSearchValue, searchHandler, setDateFrom, setDateTo, filterDate}) => {
    return (
        <>
            <div className="form-group">
                <input
                    type="text"
                    placeholder="Search..."
                    onChange={(event) => setSearchValue(event.target.value)}
                    className="input-field"
                    value={searchValue}
                />
                <button onClick={searchHandler} className="btn">Search</button>
            </div>
            <div className="form-group">
                <input
                    type="date"
                    onChange={(e) => setDateFrom(new Date(e.target.value))}
                    className="input-field"
                />
                <input
                    type="date"
                    onChange={(e) => setDateTo(new Date(e.target.value))}
                    className="input-field"
                />
                <button className="btn" onClick={filterDate}>Date filter</button>
            </div>
        </>
    );
};

export default Filtered;
