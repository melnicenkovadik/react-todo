import React from "react";

const Todo = ({todo, index, completeTodo, removeTodo}) => {
    return (
        <li style={{background: todo.isCompleted && "rgba(255, 0, 0, 0.1)"}}>
            {todo.text}
            <span>
        <button className="button" onClick={() => completeTodo(index)}>
          {todo.isCompleted ? "Completed" : "Complete"}
        </button>
        <button className="button" onClick={() => removeTodo(index)}>
          X
        </button>
      </span>
        </li>
    );
};

export default Todo;
