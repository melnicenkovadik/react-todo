import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";

import "./assets/styles/styles.css";

const app = (<App/>)
const rootElement = document.getElementById("root");
ReactDOM.render(app, rootElement);
